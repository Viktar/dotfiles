bass source /etc/profile

# Export
xdg_init

# Starfish variable
if test -z "$STARSHIP_CONFIG"
   setenv STARSHIP_CONFIG "$XDG_CONFIG_HOME/starship/starship.toml"
end

# bemenu variable
if test -z "$BEMENU_BACKEND"
   setenv BEMENU_BACKEND wayland
end

# QT platform - WA for telegram
#if test -z "$QT_QPA_PLATFORM"
#   setenv QT_QPA_PLATFORM xcb
#end

fish_add_path -gP $HOME/.local/bin/* $PATH
fish_add_path -gP $HOME/.local/bin/ $PATH


set fish_greeting                       # Supress fish into message
set EDITOR "emacsclient -t -a ''"       # Emacs in terminal
set VISUAL "emacsclient -c -a emacs"    # Emacs in GUI mode
fish_add_path -gP $HOME/.emacs.d/bin $PATH

set -x GPG_TTY (tty)                    # Set GPG tty
gpg-connect-agent updatestartuptty /bye >/dev/null

# Navigation
alias ...='cd ../..'
alias .3='cd ../../..'
alias .4='cd ../../../..'
alias .5='cd ../../../../..'

# Colorized grep
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'

# Get top process eating memory
alias psmem='ps auxf | sort -nr -k 4'
alias psmem10='ps auxf | sort -nr -k 4 | head -10'

# Get to process eating CPU
alias pscpu='ps auxf | sort -nr -k 3'
alias pscpu10='ps auxf | sort -nr -k 3 | head -10'

# CPU frequency
alias freq='watch grep \"cpu MHz\" /proc/cpuinfo'

# Gentoo update
alias gsync='$HOME/.local/bin/Fcron/gsyncJob.sh 1'
alias gupdate='$HOME/.local/bin/Fcron/gsyncJob.sh 0 1'
alias gclean='sudo emerge --depclean && sudo eclean packages && sudo eclean distfiles'

# Git dotfiles
alias config='git --git-dir=$HOME --work-tree=$HOME'

# Start SSH agent
ssh_agent

starship init fish | source

if status is-interactive
    # Commands to run in interactive sessions can go here
end
