function xdg_init
  if test -z "$XDG_CURRENT_DESKTOP"
     setenv XDG_CURRENT_DESKTOP sway
  end

  if test -z "$XDG_SESSION_DESKTOP"
     setenv XDG_SESSION_DESKTOP sway
  end

  if test -z "$XDG_CONFIG_HOME"
     setenv XDG_CONFIG_HOME $HOME/.config
  end

  if test -z "$XDG_DATA_HOME"
     setenv XDG_DATA_HOME $HOME/.local/share
  end
end
