#!/usr/bin/env -S fish --no-config
# argv[1] - 1 - getParam, 2 - setParam, 3 - sync config
# argv[2] - parameter
# argv[3] - parameter value
#

set source_path $XDG_CONFIG_HOME/wallpapers
set file_path /tmp/$USER/wallpapers
set file_config $file_path/config

function init
    if not test -e $file_config
       mkdir -p $file_path
       cp $source_path/config $file_path
    end
end

function getValue -a prm
    set wp_config (cat $file_config | sed '/\#.*$/d')
    set wp_config (string split "=" $wp_config)

    for idx in (seq (math (count $wp_config) / 2))
        if test $wp_config[(math $idx x 2 - 1)] = "$prm"
           echo $wp_config[(math $idx x 2)]
           break
        end
    end
end

function setValue -a prm val
    set val (string replace -a '/' '\/' $val)
    sed -i "s/$prm=.*\$/$prm=$val/g" $file_config
end

function makeSync
    cp $file_config $source_path
end

init

if test -z "$argv[1]"; or test $argv[1] -lt 1; or test $argv[1] -gt 3
    echo "Specify action(1 - read, 2 - save, 3 - sync)"
    return 1;
end

if test -z "$argv[2]"; and test $argv[1] -ne 3
    echo "Specify parameter name"
    return 1;
end

if test $argv[1] -eq 1
   echo (getValue $argv[2])
end

if test $argv[1] -eq 2
   setValue $argv[2] $argv[3]
end

if test $argv[1] -eq 3
   makeSync
end
