#!/usr/bin/env fish
#
function getAniNext -a inputFile
    set return

    if not test $wp_type = "animated"
       echo "Wrong wallpaper type(animated) -> $wp_type"
       return 1
    end

    set newFile (string replace -ar '\d{1,}(.*)' (date '+%-kH%M')'$1' $inputFile)
    if test (date '+%M') -lt 10
       set newFile $newFile (string replace -ar '\d{1,}(.*)' (date '+%-kH00')'$1' $inputFile)
    else
       set newFile $newFile (string replace -ar '\d{1,}(.*)' (date '+%HH')(math (date '+%M') - (math (date '+%M')%10))'$1' $inputFile)
    end

    set newFile $newFile (string replace -ar '\d{1,}(.*)' (date '+%-k')'$1' $inputFile)

    for file in $newFile
        if test -e $file
           set return $file
           break
        end
    end

    echo $return
end

if test -e "/tmp/$USER/wallpapers/config"
   set isSet 1
else
   set isSet 0
end

set wp_default_path "$HOME/Pictures/Wallpapers"
set wp_path (wall_param_gs.sh 1 "wp_path")
set wp_type (wall_param_gs.sh 1 "wp_type")
set wp_file (wall_param_gs.sh 1 "wp_file")
set wp_time (wall_param_gs.sh 1 "wp_time")
set wp_mode (wall_param_gs.sh 1 "wp_mode")
set wp_rand (wall_param_gs.sh 1 "wp_rand")
set wp_sync (wall_param_gs.sh 1 "wp_sync")
set wp_hash (echo "$wp_path$wp_type$wp_file" | md5sum)
set getNew 0

if test -z "$wp_path"
   set wp_path $wp_default_path
end

if not test $wp_type = "animated"
   and not test $wp_type = "static"
   echo "Wrong wallpaper type(static/animated) -> $wp_type."
   return 1
end

if not test $wp_mode = "random"
   and not test $wp_mode = "static"
   echo "Wrong wallpaper mode(static/random) -> $wp_mode."
   return 1
end
# Get full file name
set wp_file_full (eval "echo $wp_path/$wp_type/$wp_file")

if test -z "$wp_time"
   set getNew 1
   set $wp_time (date '+%F %T')
end

if test $getNew -eq 0
   if test $wp_mode = "random"
      if test $wp_type = "static"
         and test (math (date '+%s') - (date '+%s' -d $wp_time)) -ge (math 60 x $wp_rand)
         set getNew 1
      end

      if test $getNew -eq 0
         and test $wp_type = "animated"
         and test $isSet -eq 0
         and not test (date '+%F') = (date '+%F' -d $wp_time)
         set getNew 1
      end
   end
end

if test $getNew -eq 1
   or test $wp_type = "animated"

   if test $getNew -eq 1
      if test $wp_type = "static"
         and test $isSet -eq 1
         set wp_file_new (eval "find $wp_path/$wp_type -type f | shuf -n 1")
      else
         set wp_file_new (eval "find $wp_path -type f | shuf -n 1")
      end
   else
      set wp_file_new (getAniNext $wp_file_full)
   end

   if not test $wp_file_full = $wp_file_new
      set wp_time (date '+%F %T')
      set wp_type (string match -rg (eval "echo $wp_path")'/(.+?)/' $wp_file_new)
      set wp_file (string match -rg (eval "echo $wp_path")'/'$wp_type'/(.*)' $wp_file_new)
      set wp_file_full (eval "echo $wp_path/$wp_type/$wp_file")

      wall_param_gs.sh 2 "wp_type" $wp_type
      wall_param_gs.sh 2 "wp_file" $wp_file
      wall_param_gs.sh 2 "wp_time" $wp_time
   end
end

if test -z "$SWAYSOCK"
    set SWAY_USER (ps --no-headers -C sway -o uid:1)
    set -gx SWAYSOCK "/run/user/$SWAY_USER/sway-ipc.$SWAY_USER."(pidof sway)".sock"
end

set PID (pidof swaybg)

if test -n "$PID"
    set old_wall (readlink -f (ps -p $PID -o command= | sed 's/\(.\+-i\s\)\(\/\S\+\)\s.\+/\2/'))
end

if test -z "$old_wall"
   set old_wall "null"
end

set wp_file_new (readlink -f $wp_file_full)

if test $wp_file_new != $old_wall
   swaymsg 'output * bg '"$wp_file_new"' fill'
end

if test $wp_hash != (echo "$wp_path$wp_type$wp_file" | md5sum)
   or test $isSet -eq 0
   # Sync each 3 hours
   if test -z "$wp_sync"
      or test (math (date '+%s') - (date '+%s' -d $wp_sync)) -gt 10800
      or test $isSet -eq 0
      set wp_sync (date '+%F %T')
      wall_param_gs.sh 2 "wp_sync" $wp_sync
      wall_param_gs.sh 3
   end
end
