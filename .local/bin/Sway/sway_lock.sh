#!/bin/fish --no-config

set IMAGE "/tmp/$USER/screen.png"
set TEXT "/tmp/$USER/locktext.png"

grim -o (swaymsg -t get_outputs | jq -r '.[] | select(.focused) | .name') "$IMAGE"

convert "$IMAGE" -scale 25% -blur 0x2 -scale 400% -fill black -colorize 50% "$IMAGE"

if test -f "$argv[1]"
   convert "$IMAGE $argv[1]" -gravity center -composite -matte "$IMAGE"
end

if ! test -f "$TEST"
   convert -size 1920x60 xc:black -font "Liberation-Sans" -pointsize 26 -fill white -gravity center -annotate +0+0 'Type password to unlock' "$TEXT"
end

convert "$IMAGE" "$TEXT" -gravity center -geometry +0+200 -composite "$IMAGE"

swaylock -f -F -k -l -s fill --ring-color 272739 --ring-caps-lock-color 272739 --key-hl-color 968099 --caps-lock-key-hl-color 968099 -i "$IMAGE"
