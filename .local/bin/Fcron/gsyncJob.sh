#!/bin/fish --no-config
if test -z $argv[1]
   set isForce 0
else
   set isForce $argv[1]
end

if test -z $argv[2]
   set reUpd 0
else
   set reUpd 1
end

set reCalc 0
set reSync 0
set syncUsr (dirname (realpath (status filename)) | awk -F '/' '{print $3}')
set errFileName "/tmp/$syncUsr/emergeJobErr.txt"
set detFileName "/tmp/$syncUsr/emergeJobDet.txt"
set execSync "sudo emerge --ask=n --sync 2>$errFileName"
set execCalc "emerge --ask=n --update --deep --newuse --pretend --with-bdeps=y @system @world 2>/dev/null | egrep -o --color=never 'Total.+'>$detFileName"
set execUpd "sudo emerge --ask --verbose --update --deep --newuse --with-bdeps=y @world && true>$detFileName"
set execSyncDiffSec "math (date -u +%s) - (date -u +%s -d (emerge --info --ask=n | egrep -o --color=never 'Timestamp of repository gentoo:.+' | awk -F ': ' '{print \$2}'))"

for i in $errFileName $detFileName
   if not test -e $i
      touch $i
      chmod =666 $i

      if test "$i" = "$detFileName"
         set reCalc 1
      end
   else
      if test $isForce -eq 1
         true>$i
      end
   end
end

if test $isForce -eq 1
   set reCalc 1
   set reSync 1
   echo "Force emerge --sync with sudo"
else
   set execSync "$execSync 1>/dev/null"
   if test (eval $execSyncDiffSec) -gt 86400
      set reCalc 1
      set reSync 1
   end
end

if test $reSync -eq 1
   eval $execSync
end

if test $reUpd -eq 1
   echo "Force update system with sudo"
   eval $execUpd
   set reCalc 1
end

if test $reCalc -eq 1
   if not test -s $errFileName
      eval $execCalc
   end

   pkill -SIGRTMIN+8 waybar
end
