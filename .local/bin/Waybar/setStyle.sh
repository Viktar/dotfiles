#!/usr/bin/env -S fish --no-config
# Narayan's algorithm
# argv[1]       : Style ID
# argv[2..n]    : Theme color list
set j 1
set l 0
set styleID 1
set cfgFile $XDG_CONFIG_HOME/waybar/styleCfg.css

function getJ
    for idx in (seq $colors_length)[-1..1]
        set result (math $idx - 1)

        if test $result -eq 0
           or test $colors_idx[$result] -lt $colors_idx[$idx]
           break
        end

        set result 0
    end

    echo $result
end

if test $argv[1] -lt -1
   echo "Wrong Style ID -> $argv[1]"
   return 1
end

if test -z "$argv[2]"
   echo "Color theme is not provided"
   return 1
end

set colors $argv[2..(count $argv)]
set colors_length (count $colors)
set colors_idx (seq $colors_length)

while test $j -gt 0
      and test $argv[1] -ne 1
   set j (getJ)

   if test $j -gt 0
      for idx in (seq (math $j + 1) $colors_length)[-1..1]
        set l $idx

        if test $colors_idx[$l] -gt $colors_idx[$j]
           set tmp $colors_idx[$l]
           set colors_idx[$l] $colors_idx[$j]
           set colors_idx[$j] $tmp
           break
        end

        set l 0
      end
   end

   if test $j -gt 0
      and test $l -gt 0
      set colors_idx[(math $j + 1)..$colors_length] $colors_idx[$colors_length..(math $j + 1)]

      if test $styleID -eq $argv[1]
         break
      end

      set styleID (math $styleID + 1)
   end
end

if test $argv[1] -eq 0
   echo $styleID
end

if test $argv[1] -gt 0
   echo "" > $cfgFile

   set j 1

   for color in $colors
     echo "@define-color col$j $color;" >> $cfgFile
     echo "@define-color base0$j @col$colors_idx[$j];" >> $cfgFile

     set j (math $j + 1)
   end
end
