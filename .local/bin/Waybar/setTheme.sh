#!/usr/bin/env -S fish --no-config
#
# argv[1]      color set ID for applying
# argv[2]      color theme ID
# argv[3..n]   color list
#
function restartWaybar
    killall waybar 2>/dev/null
    killall cava 2>/dev/null
    waybar &; disown
end

set base_colors "Desert" "#46211A" "#693D3D" "#BA5536" "#A43820"
set base_colorsS (count $base_colors)
set base_colors $base_colors "Autumn" "#8D230F" "#1E434C" "#9B4F0F" "#C99E10"
set base_colors $base_colors "Warm foliage" "#2E2300" "#6E6702" "#C05805" "#DB9501"
set base_colors $base_colors "Spice" "#AF4425" "#662E1C" "#EBDCB2" "#C9A66B"
set base_colors $base_colors "Warm rustic" "#FEF2E4" "#FD974F" "#C60000" "#805A3B"
set base_colors $base_colors "Jewelry tones" "#7F152E" "#D61800" "#EDAE01" "#E94F08"
set base_colors $base_colors "Chocolate brownie" "#301B28" "#523634" "#B6452C" "#DDC5A2"
set base_colors $base_colors "Comfort and warmth" "#662225" "#B51D0A" "#EAD39C" "#7D5E3C"
set base_colors $base_colors "Wine peace" "#1E0000" "#500805" "#9D331F" "#BC6D4F"
set base_colors $base_colors "Sunny town" "#D24136" "#EB8A3E" "#EBB582" "#785A46"
set base_colors $base_colors "Mediterranean noon" "#8C0004" "#C8000A" "#E8A735" "#E2C499"
set base_colors $base_colors "Autumn orange" "#D55448" "#FFA577" "#F9F9FF" "#896E69"
set base_colors $base_colors "Golden noon" "#882426" "#CDBEA7" "#323030" "#C29545"
set base_colors $base_colors "The power of knowledge" "#42313A" "#6C2D2C" "#9F4636" "#F1DCC9"
set base_colors $base_colors "Warm and stylish" "#444C5C" "#CE5A57" "#78A5A3" "#E1B16A"
set base_colors $base_colors "Sea wave" "#003B46" "#07575B" "#66A5AD" "#C4DFE6"
set base_colors $base_colors "Green mist" "#04202C" "#304040" "#5B7065" "#C9D1C8"
set base_colors $base_colors "Water blue" "#004D47" "#128277" "#52958B" "#B9C4C9"
set base_colors $base_colors "The fog of the metropolis" "#2C4A52" "#537072" "#8E9B97" "#F4EBDB"
set base_colors $base_colors "Arctic dawn" "#FFCCBB" "#6EB5C0" "#006C84" "#E2E8E4"
set base_colors $base_colors "Blue and gray ice" "#F1F1F2" "#BCBABE" "#A1D6E2" "#1995AD"
set base_colors $base_colors "Bright Iceland" "#505160" "#68829E" "#AEBD38" "#598234"
set base_colors $base_colors "Forest after the rain" "#2E4600" "#486B00" "#A2C523" "#7D4427"
set base_colors $base_colors "Modernity" "#0F1B07" "#FFFFFF" "#5C821A" "#C6D166"
set base_colors $base_colors "Fresh greens" "#021C1E" "#004445" "#2C7873" "#6FB98F"
set base_colors $base_colors "Mountain Lake" "#324851" "#86AC41" "#34675C" "#7DA3A1"
set base_colors $base_colors "Freshness and energy" "#4CB5F5" "#B7B8B6" "#34675C" "#B3C100"
set base_colors $base_colors "Fresh greens" "#265C00" "#68A225" "#B3DE81" "#FDFFFF"
set base_colors $base_colors "Green fields" "#919636" "#524A3A" "#FFFAE1" "#5A5F37"
set base_colors $base_colors "Citrus and grass" "#EB8A44" "#F9DC24" "#4B7447" "#8EBA43"
set base_colors $base_colors "Garden freshness" "#EE693F" "#F69454" "#FCFDFE" "#739F3D"
set base_colors $base_colors "Summer fiesta" "#C7DB00" "#7AA802" "#F78B2D" "#E4B600"
set base_colors $base_colors "Natural elegance" "#EBDF00" "#7E7B15" "#563E20" "#B38540"
set base_colors $base_colors "From dusk to dusk" "#363237" "#2D4262" "#73605B" "#D09683"
set base_colors $base_colors "Salmon and rye bread" "#4B4345" "#102A49" "#F79B77" "#755248"
#21
set base_colors $base_colors "From the height of the flight" "#335252" "#D4DDE1" "#AA4B41" "#2D3033"
# 9, 14, 17, 20
set base_colors $base_colors "Amber and azure" "#90AFc5" "#336b87" "#2A3132" "#763626"
# ALL
set base_colors $base_colors "Neon sign" "#262F34" "#F34A4A" "#F1D3BC" "#615049"
set base_colors $base_colors "Exotic and durable" "#0F1F38" "#8E7970" "#F55449" "#1B4B5A"
# 14,16,18,20,22,23,24
set base_colors $base_colors "Sun and sky" "#217CA3" "#E29930" "#32384D" "#211F30"
set base_colors $base_colors "Flower meadow" "#375E97" "#FB6542" "#FFBB00" "#3F681C"
set base_colors $base_colors "Morning freshness" "#F98866" "#FF420E" "#80BD9E" "#89DA59"
set base_colors $base_colors "Tropical flower" "#F52549" "#FA6775" "#FFD64D" "#9BC01C"
set base_colors $base_colors "Summer sunflower" "#4D85BD" "#7CAA2D" "#F5E356" "#CB6318"
set base_colors $base_colors "Basket of vegetables" "#258039" "#F5BE41" "#31A9B8" "#CF3721"
set base_colors $base_colors "Lemonade stand" "#F70025" "#F7EFE2" "#F25C00" "#F9A603"
set base_colors $base_colors "Fun and tropical" "#4897D8" "#FFDB5C" "#FA6E59" "#F8A055"
set base_colors $base_colors "Cheese, figs and basil" "#4C3F54" "#D13525" "#F2C057" "#486824"
set base_colors $base_colors "Sunny citrus" "#FAAF08" "#FA812F" "#FA4032" "#FEF3E2"
set base_colors $base_colors "Apple contrast" "#F4EC6A" "#BBCF4A" "#E73F0B" "#A11F0C"
set base_colors $base_colors "Neon night" "#F77604" "#B8D20B" "#F56C57" "#231B12"
set base_colors $base_colors "The audacity of graffiti" "#F47D4A" "#E1315B" "#FFEC5C" "#008DCB"
set base_colors $base_colors "Night life" "#00CFFA" "#FF0038" "#FFCE38" "#020509"
set base_colors $base_colors "Pool party" "#344D90" "#5CC5EF" "#FFB745" "#E7552C"
set base_colors $base_colors "Painting" "#061283" "#FD3C3C" "#FFB74C" "#138D90"
set base_colors $base_colors "Cup of cappuccino" "#626D71" "#CDCDC0" "#DDBC95" "#B38867"
# 11,16,17,18,20,22,24
set base_colors $base_colors "Berry blue" "#1E1F26" "#283655" "#4D648D" "#D0E1F9"
set base_colors $base_colors "Refined simplicity" "#EAE2D6" "#D5C3AA" "#867666" "#E1B80D"
set base_colors $base_colors "Technology and nature" "#FBCD4B" "#A3A599" "#282623" "#88A550"
# 3,15
set base_colors $base_colors "City life" "#DDDEDE" "#232122" "#A5C05B" "#7BA4A8"
set base_colors $base_colors "Urban oasis" "#506D2F" "#2A2922" "#F3EBDD" "#7D5642"
# 12,13,18
set base_colors $base_colors "Modern gloss" "#2F2E33" "#D5D6D2" "#FFFFFF" "#3A5199"
set base_colors $base_colors "Neutral shabby chic" "#6C5F5B" "#CDAB81" "#DAC3B3" "#4F4A45"
# 7
set base_colors $base_colors "Classic metallic" "#080706" "#EFEFEF" "#D1B280" "#594D46"
set base_colors $base_colors "Winter colors" "#A10115" "#D72C16" "#F0EFEA" "#C0B2B5"
set base_colors $base_colors "Sea and wheat" "#00293C" "#1E656D" "#F1F3CE" "#F62A00"
set base_colors $base_colors "Original and unexpected" "#B1D7D2" "#E5E2CA" "#432E33" "#E7472E"
set base_colors $base_colors "City views" "#C5001A" "#E4E3DB" "#113743" "#C5BEBA"
set base_colors $base_colors "Industrial rigor" "#20232A" "#ACBEBE" "#F4F4EF" "#A01D26"
set base_colors $base_colors "Professionalism and power" "#962715" "#FFFFFF" "#1E1E20" "#BBC3C6"
set base_colors $base_colors "Delicate tulips" "#98DBC6" "#5BC8AC" "#E6D72A" "#F18D9E"
set base_colors $base_colors "Birds and berries" "#9A9EAB" "#5D535E" "#EC96A4" "#DFE166"
set base_colors $base_colors "Stylish retro" "#75B1A9" "#D9B44A" "#4F6457" "#ACD0C0"
set base_colors $base_colors "Summer barbecue" "#5C8F22" "#752A07" "#FBCB7B" "#EB5E30"
set base_colors $base_colors "Serenity SPA" "#A1BE95" "#E2DFA2" "#92AAC7" "#ED5752"
set base_colors $base_colors "Pastel" "#C1E1DC" "#FFCCAC" "#FFEB94" "#FDD475"
set base_colors $base_colors "Strawberries with cream" "#D8412F" "#FE7A47" "#FCFDFE" "#F5CA99"
set base_colors $base_colors "Subdued antiquity" "#A4CABC" "#EAB364" "#B2473E" "#ACBD78"
set base_colors $base_colors "Metropolis" "#8593AE" "#5A4E4D" "#7E675E" "#DDA288"
set base_colors $base_colors "Sun in the city" "#2B616D" "#B2DBD5" "#FFFFFF" "#FA8D62"
set base_colors $base_colors "Coast" "#A5C3CF" "#F3D3B8" "#E59D5C" "#A99F3C"
set base_colors $base_colors "Vintage" "#FCC875" "#BAA896" "#E6CCB5" "#E38B75"
set base_colors $base_colors "Relaxing retro" "#D35C37" "#BF9A77" "#D6C6B9" "#97B8C2"
set base_colors $base_colors "Refinement" "#5F968E" "#BFDCCF" "#E05858" "#D5C9B1"
set base_colors $base_colors "Romantic walk" "#688B8A" "#A0B084" "#FAEFD4" "#A57C65"
# 2,5,16
set base_colors $base_colors "Gentle contrast" "#FFBEBD" "#FCFCFA" "#337BAE" "#1A405F"
set base_colors $base_colors "Back to school" "#81715E" "#FAAE3D" "#E38533" "#E4535E"
set base_colors $base_colors "Smoky purple" "#A49592" "#727077" "#EED8C9" "#E99787"
set base_colors $base_colors "Hygge" "#488A99" "#DBAE58" "#4D585B" "#B4B4B4"
set base_colors $base_colors "Day and night" "#011A27" "#063852" "#F0810F" "#E6DF44"
set base_colors $base_colors "Chic without vulgarity" "#16253D" "#002C54" "#EFB509" "#CD7213"
set base_colors $base_colors "Strawberries" "#50312F" "#CB0000" "#E4EA8C" "#3F6C45"
set base_colors $base_colors "Bright night" "#000B29" "#D70026" "#F8F5F2" "#EDB83D"
set base_colors $base_colors "1950s kitchen" "#B3DBC0" "#FE0000" "#FDF6F6" "#67BACA"
set base_colors $base_colors "Urban contrasts" "#F9BA32" "#426E86" "#F8F1E5" "#2F3131"
set base_colors $base_colors "Orange accent" "#756867" "#D5D6D2" "#353C3F" "#FF8D3F"
set base_colors $base_colors "Beyond black and white" "#31A2AC" "#AF1C1C" "#F0EFFE" "#2F2F28"
set base_colors $base_colors "Surf on the sandy shore" "#F4CC70" "#DE7A22" "#20948B" "#6AB187"
set base_colors $base_colors "Greek holidays" "#2988BC" "#2F496E" "#F4EADE" "#ED8C72"
set base_colors $base_colors "Sea lights" "#257985" "#5EA8A7" "#FFFFFF" "#FF4447"
set base_colors $base_colors "Performance" "#282C34" "#F4EAED" "#F4EAED" "#45475A"

if test -n "$argv[2]";
   set colorThemeName (math $base_colorsS x $argv[2]- $base_colorsS+ 1)

   for idx in (seq (math $colorThemeName + 1) (math $base_colorsS x $argv[2]));
        set colorTheme $colorTheme $base_colors[$idx]
   end

   set colorThemeName $base_colors[$colorThemeName]
end

if test $argv[1] -eq 0;
   set maxColorSetID (setStyle.sh 0 $colorTheme)

   if test $maxColorSetID -gt 0;
      for idx in (seq 1 $maxColorSetID);
        sleep 10
        echo "color theme name: $colorThemeName"
        echo "color set ID: $idx"
        setStyle.sh $idx $colorTheme
        restartWaybar
      end
   end
else
    echo "color theme name: $colorThemeName"
    echo "color set ID: $argv[1]"

    setStyle.sh $argv[1] $colorTheme
    restartWaybar
end
