#!/bin/fish --no-config
if test -s /tmp/$USER/emergeJobErr.txt
    read -x updInfo < /tmp/$USER/emergeJobErr.txt
    echo "{\"text\":\"\", \"tooltip\":\""$updInfo"\", \"percentage\":0}"
else
    if test -s /tmp/$USER/emergeJobDet.txt
        read -x updInfo < /tmp/$USER/emergeJobDet.txt
        set updCnt (echo $updInfo | awk -F ' ' '{print $2}')

        if test $updCnt -gt 0
            echo "{\"text\":\"$updCnt updates\",\"tooltip\":\""$updInfo"\",\"percentage\":100}"
        end
    end
end
